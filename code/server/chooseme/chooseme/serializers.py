from rest_framework import serializers

from chooseme.models import Choices


class ChoicesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Choices
        fields = ['id', 'choice', 'choice_date']
