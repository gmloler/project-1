from django.contrib import admin
from django.urls import path

from chooseme import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/choices/makechoice', views.generate_new_choice),
]
