from django.db import models


class Choices(models.Model):
    id = models.AutoField(primary_key=True)
    choice = models.CharField(max_length=25)
    choice_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s' % self.choice

