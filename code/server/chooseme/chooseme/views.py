from rest_framework import status
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response


from chooseme.models import Choices
from chooseme.serializers import ChoicesSerializer


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def generate_new_choice(request):
    get_choice = request.GET.get('string', '')
    new_choice = persist_new_choice(get_choice)
    serializer = ChoicesSerializer(new_choice)
    return Response(serializer.data['choice'], status=status.HTTP_200_OK)


def persist_new_choice(get_choice):
    new_choice = Choices(choice=get_choice)
    new_choice.save()

    return new_choice
