import Axios from 'axios'

const $axios = Axios.create({
    baseURL: '/api/',
    headers: {
        'Content-Type': 'application/json'
    }
})

//Example of a cross-cutting concern - client api error-handling
$axios.interceptors.response.use(
    (response) => response,
    (error) => {
        console.error("got error")
        console.error(error)

        throw error;
    });


class ChoicesService {
    static getChoice(userString) {
        return $axios
            .get('choices/makechoice',{params:{
                    string: userString,
                }})
            .then(response => response.data)
    }
}

const service = {
    ChoicesService
}

export default service
