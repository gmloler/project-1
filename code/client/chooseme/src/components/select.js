import React from 'react';
import {useState} from "react";
import service from "../api";

function Select() {

    const [carState, setCarState] = useState("");
    const [returnValue, setReturnValue] = useState("");

    const generateNewChoiceOnClick = () => {
        service.ChoicesService.getChoice(carState).then(response => {
            setReturnValue(response);
        })
    }

    const onChange = e => {
        const changeState = e.target.value;
        setCarState(changeState);
        setReturnValue(null)
    }
    return (
        <div className={'app'}>
            <label htmlFor="cars">Choose a car </label>
            <select name="cars" id="cars" onChange={onChange}>
                <option>Select</option>
                <option value="Volvo">Volvo</option>
                <option value="Saab">Saab</option>
                <option value="Opel">Opel</option>
                <option value="Audi">Audi</option>
                <option value="Bmw">Bmw</option>
            </select>
            <button onClick={generateNewChoiceOnClick}>Click to choose</button>
            <output>{returnValue}</output>
        </div>
    );
}

export default Select;